###Prepare IL data for various subsequent mapping procedures
#Mark Sterken, 17/8/2020

###input
# trait.list (a dataframe with the columns strain, trait, and value; the column 'genetic_background' is optional, in case of N2 and CB4856 WUR ILs it will be added by the function)
# strain.map (the genetic map of the population, in matrix form, strains in columns, markers in rows)
# strain.marker (info on the genetic markers used: name, chromosome and position)

###output
#a list with with the entries: Trait_original, Map_original, Marker

###Description
#The function adds the genetic background type to the ILs and trims the map to the IL found in the data.

###See also     
#IL.map.bin1BG; IL.map.bin2BG; IL.map.Individual 


IL.data.prep <- function(trait.list,strain.map,strain.marker){
                         if(missing(trait.list)){                               stop("requires trait list, strain, genetic_background, trait, value")}
                             colnames(trait.list) <- tolower(colnames(trait.list))
                         if(missing(strain.map)){                               stop("requires the genetic map of the population")}
                         if(sum(tolower(trait.list$strain) %in% tolower(colnames(strain.map))) < nrow(trait.list)){
                                                                                stop("not all strains are in the genotype matrix")}
                         if(missing(strain.marker)){                            stop("requires marker info: name, chromosome, position")}
                         if(ncol(strain.marker) != 3){                          stop("strain.marker should contain marker name, chromosome and position")}
                         if(ncol(strain.marker) == 3){                          colnames(strain.marker) <- c("name","chromosome","position")}                             

                         trait.list <- data.frame(trait.list) %>%
                                       mutate(strain=as.character(unlist(strain)),
                                              value=as.numeric(as.character(unlist(value))),
                                              trait=as.character(unlist(trait)))
                                             
                         if(!"genetic_background" %in% colnames(trait.list)){
                             trait.list <- mutate(trait.list,genetic_background=ifelse(tolower(strain) %in% c(paste("ewir","00",1:9,sep=""),paste("ewir","0",10:90,sep="")),"N2",
                                                                                  ifelse(tolower(strain) %in% paste("wn",201:290,sep=""),"N2",
                                                                                    ifelse(tolower(strain) %in% paste("wn",301:387,sep=""),"CB4856",NA))))
                         }

                         ###Make matrix
                         map.nu <- strain.map[,tolower(colnames(strain.map)) %in% unique(tolower(trait.list$strain))]
                             rownames(map.nu) <- rownames(strain.map)

                         ###Make output
                         output <- list(NULL)
                         output[[1]] <- trait.list
                         output[[2]] <- map.nu
                         output[[3]] <- strain.marker
                         names(output) <- c("Trait_original","Map_original","Marker")

                         ###return output
                         return(output)
                        }
