###emma package
#Kang, Hyun Min, et al. "Efficient control of population structure in model organism association mapping." Genetics 178.3 (2008): 1709-1723.
#emma 1.12


emma.eigen.L.wo.Z <- function (K)
{
    eig <- eigen(K, symmetric = TRUE)
    return(list(values = eig$values, vectors = eig$vectors))
}
